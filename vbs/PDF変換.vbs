Option Explicit
 
Dim fp
Dim fso
Dim args
Dim i
 
Const msoFalse = 0
Const msoTrue = -1
Const xlTypePDF = 0
Const xlQualityStandard = 0
Const wdExportFormatPDF = 17
Const wdExportOptimizeForPrint = 0
Const wdExportAllDocument = 0
Const wdExportDocumentContent = 0
Const wdExportCreateWordBookmarks = 2
Const wdDoNotSaveChanges = 0
Const ppSaveAsPDF = 32
 
Set fso = CreateObject("Scripting.FileSystemObject")
Set args = WScript.Arguments
 
If args.Count < 1 Then
  MsgBox "このスクリプトにファイルをドラッグして使ってね。", vbExclamation + vbSystemModal
  WScript.Quit
End If
 
For i = 0 To args.Count - 1
  fp = fso.GetParentFolderName(args(i)) & ChrW(92) & fso.GetBaseName(args(i)) & ".pdf"
  Select Case LCase(fso.GetExtensionName(args(i)))
    Case "doc","docx","docm","dot","dotx","dotm"
      With CreateObject("Word.Application")
        .Visible = True
        With .Documents.Open(args(i))
            .ExportAsFixedFormat fp, wdExportFormatPDF, False, wdExportOptimizeForPrint, wdExportAllDocument, , , _
                wdExportDocumentContent, False, False, wdExportCreateWordBookmarks, True, True, False
            .Close wdDoNotSaveChanges
        End With
        .Quit
      End With
    Case "xls","xlsx","xlsm","xlsb","xlt","xltx","xltm"
      With CreateObject("Excel.Application")
        .Visible = True
        With .Workbooks.Open(args(i))
                .ExportAsFixedFormat xlTypePDF, fp, xlQualityStandard, False, False, , , False
            .Close False
        End With
        .Quit
      End With
    Case "ppt","pptx","pptm","pot","potx","potm"
      With CreateObject("PowerPoint.Application")
        .Visible = True
        With .Presentations.Open(args(i))
            .SaveAs fp, ppSaveAsPDF, msoTrue
            .Close
        End With
        .Quit
      End With
  End Select
Next
 
MsgBox "変換完了！", vbInformation + vbSystemModal
